from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name="index"),
    path('films/liste',views.liste_films,name="liste_films"),
    path('films/detail/<int:id>',views.detail_film,name="detail_film"),
    path('film/modifier/<int:id>',views.modifier_film,name="modifier_film"),
    path('film/ajouter',views.ajouter_film,name="ajouter_film"),
    path('film/supprimer/<int:id>',views.supprimer_film,name="supprimer_film"),

    path('realisateurs/liste',views.liste_realisateur,name="liste_realisateurs"),
    path('realisateurs/detail/<int:id>',views.detail_realisateur,name="detail_realisateur"),
    path('realisateurs/modifier/<int:id>',views.modifier_realisateur,name="modifier_realisateur"),
    path('realisateurs/ajouter',views.ajouter_realisateur,name="ajouter_realisateur"),
    path('realisateurs/supprimer/<int:id>',views.supprimer_realisateur,name="supprimer_realisateur"),

]
