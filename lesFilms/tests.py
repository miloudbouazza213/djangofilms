from django.test import TestCase
from lesFilms.models import Film , Realisateur
from django.urls import reverse
# Create your tests here.


class CreationTest(TestCase) :

    #test creation d'un user
    def test_creation_realisateur(self):
        Realisateur.objects.create(nom="bouazza")
        realisateur = Realisateur.objects.get(nom = "bouazza")
        self.assertEqual(realisateur.nom,"bouazza")

    #test creation d'une tache 
    def test_creation_film(self):
        Realisateur.objects.create(nom="bouazza")
        realisateur = Realisateur.objects.get(nom = "bouazza")
        Film.objects.create(titre ="test",description="film de test",duree = "150 min",image_url="#",annee_sortie=2000,classification="10",realisateur=realisateur,genres="test" )
        film = Film.objects.get(titre="test")
        self.assertEqual(film.titre ,"test")


class RoutesTest(TestCase):

    #test de la route nommée home qui doit renvoyé / 
    def test_route_index_renvoie_slash(self):
        url = reverse("index")
        self.assertEqual("/",url)

    #test de route editUser avec l'id 3 qui doit renvoyer /user/edit/3
    def test_route_modifierRealisateur(self):
        url = reverse("modifier_realisateur" , args=[3])
        self.assertEqual(url, "/realisateurs/modifier/3")

    #test de la route qui affiche la liste des tache d'un utilisateur d'id 1
    def test_route_detail_film(self):
        url = reverse("detail_film",args=[1])
        self.assertEqual('/films/detail/1',url)


