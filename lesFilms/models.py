from django.db import models
from django.forms import ModelForm,Textarea

# Create your models here.
class Realisateur(models.Model):
    nom = models.CharField(max_length=100)

    #pour afficher le nom de l'objet et nom Object(..)
    def __str__(self):
        return self.nom

class Film(models.Model):
    titre = models.CharField(max_length=250)
    description = models.TextField(max_length=1500)
    
    #chaine de caratere montrant les durée en minute:
    duree = models.CharField(max_length=10)
    #url de l'image car les image ne sont pas stocké sur notre base
    image_url = models.CharField(max_length=500)
    #annee en entier car le scraping retourne que l'annee donc pas besoin de faire un format date pour notre site
    annee_sortie = models.IntegerField(null=True)
    classification = models.CharField(max_length=50,null=True)
    realisateur = models.ForeignKey(Realisateur, on_delete=models.CASCADE)
    #realisateur = models.CharField(max_length=250)
    genres = models.CharField(max_length=250)



#creation de formulaire 

class RealisateurForm(ModelForm):
    class Meta:
        model = Realisateur
        fields = ("nom",)


class FilmForm(ModelForm):
    class Meta:
        model = Film
        fields = '__all__'
        widgets = {'description': Textarea(attrs={'cols':60,'rows':3}),}




    