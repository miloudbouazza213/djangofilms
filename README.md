# DjangoFilms

# analyse
site qui affiche des films est leurs information à partir du site imdb.com :
on a choisi de faire un scraping sur les page des 1000 meilleurs film

modele conceptuel de données : 

v1:
![alt text](./analyse/mcd.svg)

# installation et commande
## creer virtual env 
virtualenv -p python3 venv

## installation des dependance,remplissage de la base de donnée et lancement du serveur
## (fait automatiquement au lancement du gitpod voir .gitpod.yml)
pip install -r requirements.txt

yarn add bootstrap jquery popper.js (pour l'affichage)
mv node_modules/ lesFilms/static/
### lancer le scraping 
executer le script lesFilms/scrap.py
python3 lesFilms/scrap.py

### charger les donnée des fichier json dans la base de donnee
 les realisateurs avant les film !
 python3 manage.py loaddata realisateurs.json
 python3 manage.py loaddata films.json

### lancer le serveur 
  python3 manage.py runserver

## creer superuser si besoin
python3 manage.py createsuperuser

# Test
python3 manage.py test (nom module, nom packet, nom test ou methode)
## test creation obj
python3 manage.py test 
python3 manage.py test lesFilms.tests.CreationTest
python3 manage.py test lesFilms.tests.RoutesTest
  # reste a faire :
  chercher solution pour accelerer le scraping 
  
