from django.contrib import messages
from django.shortcuts import redirect, render
from django.core.paginator import Paginator
from lesFilms.models import Film ,Realisateur ,FilmForm ,RealisateurForm


# Create your views here.

def index(request):
    #recuperation des 3 derniers films : 
    films =  Film.objects.all().order_by("-annee_sortie")[:10]
    return render(request, template_name="index.html", context={"films": films})


###### FILM :

def liste_films(request):
    #liste de tout les films
    films =  Film.objects.all().order_by('titre')
    paginator = Paginator(films, 25) # montre 25 film par page
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, template_name="liste_films.html", context={'page_obj': page_obj})

def detail_film(request,id):
    #detail d'un film
    film = Film.objects.get(pk=id)
    return render(request,template_name="detail_film.html",context={"film":film})


def modifier_film(request,id):
    #modifier film d'pk id
    film = Film.objects.get(pk=id)
    #si post verifie et modifie:
    if request.method == "POST":
        film_form= FilmForm(request.POST,instance=film)
        if film_form.is_valid():
            film_form.save()
            # on renvoi à une page detail : 
            return render(request,template_name="detail_film.html",context={"film":film})

    #si get presente formulaire
    film_form = FilmForm(instance=film)
    return render(request,template_name="modifier_film.html",context={"film_form":film_form,"film":film})


def ajouter_film(request):
    film_form = FilmForm()
    #if post verifie et ajoute
    if request.method == "POST":
        film_form = FilmForm(request.POST)
        if film_form.is_valid():
            new_film = film_form.save()
            #renvoi à la page detail:
            return render(request,template_name="detail_film.html",context={"film":new_film})
    
    #si get affiche form
    return render(request,template_name="ajout_film.html",context={"film_form":film_form})


def supprimer_film(request,id):
    film =Film.objects.get(pk=id)
    film.delete()
    message = "le film "+ film.titre +" a été supprimé"
    messages.success(request, message)
    return redirect('/')





##### REALISATEUR :

def liste_realisateur(request):
    #liste des realisateur
    realisateurs = Realisateur.objects.all().order_by('nom')
    paginator = Paginator(realisateurs, 25) # montre 25 film par page
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request,template_name="liste_realisateurs.html",context={"page_obj":page_obj})

def detail_realisateur(request,id):
    #detail du realisateur et ses films
    realisateur = Realisateur.objects.get(pk = id)
    return render(request,template_name="detail_realisateur.html",context={"realisateur":realisateur})


def modifier_realisateur(request,id):
    realisateur = Realisateur.objects.get(pk = id)
     #si post verifie et modifie:
    if request.method == "POST":
        realisateur_form = RealisateurForm(request.POST,instance=realisateur)
        if realisateur_form.is_valid():
            realisateur_form.save()
            #renvo ia page detail
            return render(request,template_name="detail_realisateur.html",context={"realisateur":realisateur})
    #si get presente formulaire
    realisateur_form = RealisateurForm(instance=realisateur)
    return render(request,template_name="modifier_realisateur.html",context={"realisateur_form":realisateur_form,"realisateur":realisateur})



def ajouter_realisateur(request):
    realisateur_form = RealisateurForm()
    #si post ajoute realisateur
    if request.method == "POST":
        realisateur_form = RealisateurForm(request.POST)
        if realisateur_form.is_valid():
            new_realisateur = realisateur_form.save()
            #renvoi à la page detail:
            return render(request,template_name="detail_realisateur.html",context={"realisateur":new_realisateur})
    
    #si get affiche form
    return render(request,template_name="ajout_realisateur.html",context={"realisateur_form":realisateur_form})


def supprimer_realisateur(request,id):
    realisateur = Realisateur.objects.get(pk=id)
    realisateur.delete()
    message = "le realisateur " + realisateur.nom +" a été supprimé"
    messages.success(request, message)
    return redirect('/')