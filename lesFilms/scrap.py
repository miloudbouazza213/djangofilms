import requests 
import re
from bs4 import BeautifulSoup
#pour randomiser le temps d'attente entre scraping de chaque page
from time import sleep
from random import randint, random
print("-------------------- Scrapping des films --------------------------------")
#count = 250 (max) affiche 250film par page pour recuperer le maximum de film en moins de page
url = "https://www.imdb.com/search/title/?count=250&groups=top_1000&sort=user_rating"

#dans url start=nombre => debut où le count commence :
pages =[
    "&start=0",
    "&start=251",
    "&start=501",
    "&start=751",
] 
#pour empecher les changement entre chaque page
param_supp = "&ref_=adv_nxt" 
#pour avoir les titre des films en francais:
headers = {"Accept-Language": "fr-FR, fr;q=0.9"}

#dictionnaire
films=[]
liste_realisateurs=[]
#div recupéré sur les page

for page in pages:
    result= requests.get(url+page+param_supp,headers=headers)
    html = result.text
    soup = BeautifulSoup(html,"html.parser")
    listes = soup.find_all("div", {"class": "lister-item mode-advanced"}) 

    print("scraping de la page " + page)


    # recuperer les realisateur:
    for liste in listes:
        #realisateur
        nom_realisateur = description=liste.select_one("p:nth-of-type(3)").a.text
        #remplir la liste de nom de realisateur:
        if nom_realisateur not in liste_realisateurs:
            liste_realisateurs.append(nom_realisateur)
    
    #recuperer les films
    for liste in listes:

        #recuperation donnée:
        #img
        image = liste.img['loadlate']
        #titre
        titre=liste.find("h3").a.text
        #date
        date=liste.select_one("h3  span:nth-of-type(2)").text
        #classification
        if liste.find("span",{"class":"certificate"}) != None:
            classification = liste.find("span",{"class":"certificate"}).text
        else :
            classification =""
        #duree
        duree = liste.find("span",{"class":"runtime"}).text
        #genre
        genre = liste.find("span",{"class":"genre"}).text.strip()
        #description
        description=liste.select_one("p:nth-of-type(2)").text

        #realisateur
        nom_realisateur =liste.select_one("p:nth-of-type(3)").a.text

        #index realisateur
        ind_realisateur = liste_realisateurs.index(nom_realisateur)+1

        #ajout de film dans le tableau :
        film ={
            "model": "lesFilms.film",
            "fields": {
                "titre": titre,
                "description": description,
                "duree": duree,
                "image_url": image,
                "annee_sortie": int(re.search(r'\d+', date).group()) ,
                "classification": classification,
                "realisateur": ind_realisateur,
                "genres": genre
            }
        }
        films.append(film)



    # #tattendre entre 0 et 1s entre le scraping de chaque page
    # sleep(random())





#ajouter l'object realisateur au bon format
realisateurs = []
i=0
for r in liste_realisateurs:
    i +=1
    realisateur = {
        "model": "lesFilms.realisateur",
        "pk": i ,
        "fields": {
            "nom": r,
        }
    }
    realisateurs.append(realisateur)


# creation json 
import json

# Serializing json
with open("films.json", "w"  ,encoding='utf-8') as outfile:
    json.dump(films,outfile , ensure_ascii=False)

# Serializing json
with open("realisateurs.json", "w"  ,encoding='utf-8') as outfile:
    json.dump(realisateurs,outfile , ensure_ascii=False)


    print("-------------------- fin du scraping --------------------------------")

