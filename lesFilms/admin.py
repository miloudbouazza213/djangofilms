from django.contrib import admin
from lesFilms.models import Film ,Realisateur

# Register your models here.


class Filmadmin(admin.ModelAdmin):
    list_display=('titre','annee_sortie')

class Realadmin(admin.ModelAdmin):
    list_display=("id","nom")
admin.site.register(Film,Filmadmin)
admin.site.register(Realisateur,Realadmin)
    